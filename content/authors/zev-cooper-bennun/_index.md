---
name: "Zev"
---

**About Me**: 

As I write this, I am the Secretary of SUCS. I'm an amateur writer, and living proof that you don't need to study Computer Science to join SUCS. Go to my Twitter if you want some hot takes.

Interests:
* Current Affairs
* Radical Politics
* Writing
* Analogue Photography 
* Open-Source Hardware
* Micromanufacturing
