---
title: "A Brief Hello World"
date: 2020-09-23T16:17:26+01:00
draft: false
toc: false # Table of contents
authors:
  - Zev
tags:
  - news
  - information
---

Hi! This is the first blog post on the new microsite. <!--more-->Our overall aim with this project is threefold - to have an open-source site for people to work on and maintain, a space for SUCS members to interact and discuss topics at length, and (following from that) an information repository that can be looked back on in the future, for finding resources and viewpoints on a variety of topics. 

The blog will contain news about SUCS, pertaining to both specific projects and the general direction of the society. Alongside this however, it'll be a place for members to write down their thoughts on other topics, such as tech news, thoughts about the future, projects that are being worked on, etc. Anyone can write for the site, and anyone can work to improve it. 

Every month or so, we hope to hold a symposium on a given (general) topic. Anyone can write a feature article for the symposium, and anyone will be able to respond to any of those feature articles, with their own thoughts, rebukes, and agreements. The articles don't have to be long, and we encourage anyone to take part, even those with no former writing experience. 

Alongside this, we will be working on other projects, contained within the blog and outside of it, that will contain updated guides and documentation for our resources - this will include existing documentation transferred and updated from our existing website, and newly written guides. It is our aim to have this information easily and readily accessible for all members who need it.

The site is currently in its early stages - before it can start flourishing, we need people to work on adding new features, maintaining existing ones, and working on the overall design and theme of the site. And, of course, we need people to be interested in writing.

It is built in Hugo, a static site generator. This makes sense for a content heavy site, as it generates HTML from Markdown formatted text, rather than having to build anything complicated and/or resource heavy. Despite the obvious limitations of a static site, it's still possible to create rich functionality - e.g. sorting by post tags or author, as well as a symposium style format of linked articles. This also ties in nicely to our existing infrastructure, as new posts can be created by simply opening a merge request with the article, on [our Gitlab](https://projects.sucs.org). When merge requests are accepted, the site will automatically be updated using Continuous Integration.

We hope the site can be expanded to add more useful functionality, and become a showcase of how simple community driven development can create uncomplicated resources that can be used to the benefit of everyone.


